/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Pavilion
 */
public class Principal {

    public static ArrayList<Proyecto> listaProyectos = new ArrayList<>();
    public static ArrayList<Empleado> listaEmpleadosAlterno = new ArrayList<>();
    static Scanner leer = new Scanner(System.in);

    public static void main(String[] args) {
        int opcion = 0;
        Empleado empleado1;
        Proyecto proyecto1;
        String nya, tel, email, nombreProy;
        int dni, dniEliminar, id, idEliminar, idAgregar, dniAgregar, idMostrar, idMostrar1, dniEliminar1, idBuscar;
        double inversion;

        try {
            do {
                System.out.println("=============================");
                System.out.println("MUNICIPALIDAD DE LA QUIACA\n"
                        + "1) Empleados\n"
                        + "2) Proyectos\n"
                        + "3) Salir\n"
                        + "Ingrese una Opcion");
                opcion = leer.nextInt();
                switch (opcion) {
                    case 1:
                        int resp = 0;
                        try {
                            do {
                                System.out.println("");
                                System.out.println("===================");
                                System.out.println("SECCION EMPLEADOS\n"
                                        + "1) Agregar Empleado\n"
                                        + "2) Eliminar Empleado\n"
                                        + "3) Listar Empleados\n"
                                        + "4) Volver\n"
                                        + "Ingrese una Opcion");
                                resp = leer.nextInt();
                                switch (resp) {
                                    case 1:
                                        try {
                                            System.out.println("Agregando Empleado");
                                            System.out.println("Ingrese DNI");
                                            dni = leer.nextInt();
                                            leer.nextLine();
                                            System.out.println("Ingrese Apellido y Nombre");
                                            nya = leer.nextLine();
                                            System.out.println("Ingrese Telefono");
                                            tel = leer.nextLine();//leer.nextLine();
                                            System.out.println("Ingrese Email");
                                            email = leer.nextLine();//leer.nextLine();
                                            empleado1 = new Empleado(nya, dni, tel, email);
                                            listaEmpleadosAlterno.add(empleado1);
                                        } catch (NumberFormatException n) {
                                            System.out.println("No es numero");
                                        } catch (Exception ex) {
                                            System.out.println(ex.getMessage());
                                        }
                                        break;

                                    case 2:
                                        boolean encontrado = false;
                                        System.out.println("Ingrese el Dni del Empleado a eliminar");
                                        dniEliminar = leer.nextInt();
                                        leer.nextLine();
                                        for (int i = 0; i < listaEmpleadosAlterno.size(); i++) {
                                            if (listaEmpleadosAlterno.get(i).getDni() == dniEliminar) {
                                                listaEmpleadosAlterno.remove(i);
                                                encontrado = true;
                                            }
                                        }
                                        if (encontrado) {
                                            System.out.println("Se Elimino al Empleado ");
                                        } else {
                                            System.out.println("No se encontro coincidencias.");
                                        }

                                        break;

                                    case 3:
                                        if (listaEmpleadosAlterno.isEmpty()) {
                                            System.out.println("La lista esta vacia");
                                        } else {
                                            Collections.sort(listaEmpleadosAlterno);
                                            for (int i = 0; i < listaEmpleadosAlterno.size(); i++) {
                                                System.out.println(listaEmpleadosAlterno.get(i));
                                            }
                                            System.out.println("");
                                            System.out.println("Total de Empleados " + listaEmpleadosAlterno.size());
                                        }
                                        break;

                                    case 4:
                                        break;
                                    default:
                                        System.out.println("Opcion no disponible.");
                                        break;

                                }
                            } while (resp != 4);
                        } catch (InputMismatchException ex) {
                            System.out.println("Debes insertar un numero");
                        }

                        break;
                    case 2:
                        int op = 0;
                        try {
                            do {
                                System.out.println("");
                                System.out.println("====================");
                                System.out.println("SECCION PROYECTOS\n"
                                        + "1) Agregar Proyecto\n"
                                        + "2) Eliminar Proyecto\n"
                                        + "3) Listar Proyectos\n"
                                        + "4) Agregar Empleado a un Proyecto\n"
                                        + "5) Quitar Empleado de un Proyecto\n"
                                        + "6) listar Datos de un Proyecto\n"
                                        + "7) Calcular total de montos destinados a proyectos\n"
                                        + "8) Volver\n"
                                        + "Ingrese una Opcion");
                                op = leer.nextInt();
                                switch (op) {

                                    case 1:

                                        try {
                                            System.out.println("Ingrese ID");
                                            id = leer.nextInt();
                                            leer.nextLine();
                                            System.out.println("Ingrese Inversion");
                                            inversion = leer.nextDouble();
                                            leer.nextLine();
                                            System.out.println("Nombre del Proyecto");
                                            nombreProy = leer.nextLine();

                                            proyecto1 = new Proyecto(id, nombreProy, inversion);
                                            listaProyectos.add(proyecto1);
                                        } catch (NumberFormatException n) {
                                            System.out.println("No es numero");
                                        } catch (Exception ex) {
                                            System.out.println(ex.getMessage());
                                        }
                                        break;

                                    case 2:

                                        boolean encontrado = false;
                                        System.out.println("Ingrese el ID del Proyecto a eliminar");
                                        idEliminar = leer.nextInt();
                                        leer.nextLine();
                                        for (int i = 0; i < listaProyectos.size(); i++) {
                                            if (listaProyectos.get(i).getId() == idEliminar) {
                                                listaProyectos.remove(i);
                                                encontrado = true;
                                            }
                                        }
                                        if (encontrado) {
                                            System.out.println("Se Elimino el Proyecto ");
                                        } else {
                                            System.out.println("No se encontro coincidencias.");
                                        }
                                        break;

                                    case 3:

                                        if (listaProyectos.isEmpty()) {
                                            System.out.println("La lista esta vacia");
                                        } else {
                                            for (int i = 0; i < listaProyectos.size(); i++) {
                                                System.out.println(listaProyectos.get(i));
                                            }
                                            System.out.println("");
                                            System.out.println("Total de Proyectos " + listaProyectos.size());
                                        }
                                        break;

                                    case 4:

                                        if (listaProyectos.isEmpty()) {
                                            System.out.println("La lista esta vacia");
                                            System.out.println("Debe agregar un proyecto nuevo");
                                        } else {
                                            for (int i = 0; i < listaProyectos.size(); i++) {
                                                System.out.println(listaProyectos.get(i));
                                            }
                                            System.out.println("Ingrese ID del Proyecto donde se agregara el Empleado");
                                            idAgregar = leer.nextInt();
                                            leer.nextLine();

                                            for (int i = 0; i < listaProyectos.size(); i++) {
                                                if (listaProyectos.get(i).getId() == idAgregar) {

                                                    System.out.println("Mostrando Empleados para Seleccionar");
                                                    if (listaEmpleadosAlterno.isEmpty()) {
                                                        System.out.println("La lista de empleados esta vacia");
                                                        System.out.println("Debe agregar empleados");
                                                    } else {
                                                        Collections.sort(listaEmpleadosAlterno);
                                                        for (int j = 0; j < listaEmpleadosAlterno.size(); j++) {
                                                            System.out.println(listaEmpleadosAlterno.get(j));
                                                        }
                                                        System.out.println("Ingrese DNI del Empleado a Agregar");
                                                        dniAgregar = leer.nextInt();

                                                        for (int x = 0; x < listaEmpleadosAlterno.size(); x++) {
                                                            if (listaEmpleadosAlterno.get(x).getDni() == dniAgregar) {

                                                                listaProyectos.get(i).agregarEmpleadoaProyecto(listaEmpleadosAlterno.get(x));
                                                                System.out.println("El empleado se agrego al proyecto!!");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        break;

                                    case 5:

                                        if (listaProyectos.isEmpty()) {
                                            System.out.println("La lista esta vacia");
                                            System.out.println("Debe agregar un proyecto nuevo");
                                        } else {
                                            for (int i = 0; i < listaProyectos.size(); i++) {
                                                System.out.println(listaProyectos.get(i));
                                            }
                                            System.out.println("Ingrese ID del Proyecto para mostrar sus datos");
                                            idMostrar1 = leer.nextInt();
                                            leer.nextLine();
                                            
                                            try{
                                            for (int i = 0; i < listaProyectos.size(); i++) {

                                                if (listaProyectos.get(i).getId() == idMostrar1) {
                                                    listaProyectos.get(i).listarDatosProyecto();
                                                    System.out.println("Ingrese Dni del empleado a eliminar");
                                                    dniEliminar1 = leer.nextInt();
                                                    leer.nextLine();
                                                    
                                                    if (listaProyectos.get(i).getListaEmpleados().get(i).getDni() == dniEliminar1) {
                                                        listaProyectos.get(i).eliminarEmpleadodeProyecto(listaProyectos.get(i).getListaEmpleados().get(i));
                                                    }
                                                }
                                            }
                                            }catch(IndexOutOfBoundsException ex){
                                                System.out.println("Dni equivocado");
                                            }
                                            System.out.println("Empleado Eliminado");
                                        }

                                        break;

                                    case 6:

                                        if (listaProyectos.isEmpty()) {
                                            System.out.println("La lista esta vacia");
                                            System.out.println("Debe agregar un proyecto nuevo");
                                        } else {
                                            for (int i = 0; i < listaProyectos.size(); i++) {
                                                System.out.println(listaProyectos.get(i));
                                            }
                                            System.out.println("Ingrese ID del Proyecto para mostrar sus datos");
                                            idMostrar = leer.nextInt();
                                            leer.nextLine();

                                            for (int i = 0; i < listaProyectos.size(); i++) {

                                                if (listaProyectos.get(i).getId() == idMostrar) {
                                                    listaProyectos.get(i).listarDatosProyecto();
                                                }
                                            }
                                        }

                                        break;

                                    case 7:
                                        double total = 0;
                                        for (int i = 0; i < listaProyectos.size(); i++) {
                                            total = total + listaProyectos.get(i).getInversion();
                                        }
                                        System.out.println("Total de montos destinados a proyectos = " + total);
                                        break;

                                    case 8:
                                        System.out.println("");
                                        break;
                                    default:
                                        System.out.println("Opcion no disponible.");
                                        break;

                                }
                            } while (op != 8);
                        } catch (InputMismatchException ex) {
                            System.out.println("Debes insertar un numero");
                        } finally{}

                        break;

                    case 3:
                        System.out.println("Fin del Programa.");
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Opcion no disponible.");
                        break;
                }
            } while (opcion != 3);
        } catch (InputMismatchException ex) {
            System.out.println("Debes insertar un numero");
        }
        
    }//cierra el main

}
