/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;


/**
 *
 * @author Pavilion
 */
public class Empleado implements Comparable<Empleado>{
    private String nombreyApellido;
    private int dni;
    private String telefono;
    private String email;

    public Empleado() {
    }

    public Empleado(String nombreyApellido, int dni, String telefono, String email) {
        this.nombreyApellido = nombreyApellido;
        
        this.dni = dni;
        this.telefono = telefono;
        this.email = email;
    }

    public String getNombreyApellido() {
        return nombreyApellido;
    }

    public void setNombreyApellido(String nombreyApellido) {
        this.nombreyApellido = nombreyApellido;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        System.out.println("_________________________________________________________________________");//60
        return "|Nombre y Apellido: " + this.nombreyApellido + "  |DNI: " + this.dni + "  |Tel.: "
                + this.telefono + "  |Email: " + this.email + "  |";
        
    }

    @Override
    public int compareTo(Empleado t) {
        String a = new String(String.valueOf(this.getNombreyApellido()));
        String b = new String(String.valueOf(t.getNombreyApellido()));
        return a.compareTo(b);
    }
    
    
    
    
    
}
