/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;
import clases.Empleado;
import java.util.ArrayList;

/**
 *
 * @author Pavilion
 */
public class Proyecto {
    private int id;
    private String nombre;
    private double inversion;
    private ArrayList<Empleado> listaEmpleados= new ArrayList();

    public Proyecto(int id, String nombre, double inversion) {
        this.id = id;
        this.nombre = nombre;
        this.inversion = inversion;
    }

    public ArrayList<Empleado> getListaEmpleados() {
        return listaEmpleados;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getInversion() {
        return inversion;
    }

    public void setInversion(double inversion) {
        this.inversion = inversion;
    }

    @Override
    public String toString() {
         System.out.println("_________________________________________________________________________");
        return "| Id: " + id + "  | Nombre: " + nombre + "  | Inversion: " + inversion + "  |";
    }
    
    public void agregarEmpleadoaProyecto(Empleado empleado1){
        if(listaEmpleados.contains(empleado1)){
            System.out.println("El empleado ya esta agregado");
        }else{
        listaEmpleados.add(empleado1);}
    }
    
    public void eliminarEmpleadodeProyecto(Empleado empleado2){
        listaEmpleados.remove(empleado2);
    }
    
    public void listarDatosProyecto(){
        for(int i=0;i<listaEmpleados.size();i++){
            System.out.println(listaEmpleados.get(i));
        }
        System.out.println("Total de Empleados: "+listaEmpleados.size());
    }
    
    
}
